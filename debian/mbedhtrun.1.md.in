% MBEDHTRUN(1) __VERSION__ | User Commands
% 
% December 2018

# NAME

**mbedhtrun** - flash, reset and perform host-supervised tests on Mbed platforms

# SYNOPSIS

| **mbedhtrun** **\-\-image-path** _FLASH\_FILE_ **\-\-micro** _MICRO_
|           **\-\-port** _PORT_ **\-\-disk** _TARGET\_PATH_
|          \[**\-\-target-id** _TARGET\_ID_\] \[**\-\-sync** _SYNC_\]
|          \[**\-\-sync-timeout** _TIMEOUT_\] \[**\-\-copy** _METHOD_\]
|          \[**\-\-retry-copy** _ATTEMPTS_\] \[**\-\-tag-filters** _FILTERS_\]
|          \[**\-\-reset**\] \[**\-\-program\_cycle\_s** _TIMEOUT_\]
|          \[**\-\-reset-timeout** _TIMEOUT_\] \[**\-\-process-start-timeout** _TIMEOUT_\]
|          \[**\-\-polling-timeout** _TIMEOUT_\]
|          \[**\-\-enum-host-tests** _DIR_\] \[**\-\-test-cfg** _FILE_\]
|          \[**\-\-run**\] \[**\-\-skip-flashing**\] \[**\-\-skip-reset**\]
|          \[**\-\-send-break**\] \[**\-\-baud-rate** _BAUD\_RATE_\]
|          \[**\-\-serial-output-file** _FILE_\] \[**\-\-compare-log** _FILE_\]
|          \[**\-\-verbose**\]

| **mbedhtrun** **\–\-list**

| **mbedhtrun** **\–\-plugins**

| **mbedhtrun** **\–\-version**

| **mbedhtrun** **\–\-help**

# DESCRIPTION

**mbedhtrun** supports flashing, resetting and testing of Mbed Enabled devices
from the command line.

Whilst test automation tools such as **mbed-test-wrapper** can provide
automated flashing and testing of Mbed devices, **mbedhtrun** provides a simple
and convenient CLI to flash, reset and test devices directly.

# OPTIONS

-f, \-\-image-path _FLASH\_FILE_
:   path to file to flash

-m, \-\-micro _MICRO_
:   target microcontroller name

-p, \-\-port _PORT_
:   serial port of the target

-d, \-\-disk _TARGET\_PATH_
:   target disk (mount point) path

-t, \-\-target-id _TARGET\_ID_
:   unique Target ID or Mbed platform

\-\-sync _SYNC_
:   how many times \_\_sync packet will be sent to device (0: none; -1: forever; 1,2,3...: number of times; default: 2)

\-\-sync-timeout _TIMEOUT_
:   delay in seconds between \_\_sync packet (default: 5)

-c, \-\-copy _METHOD_
:   copy (flash the target) method selector

\-\-retry-copy _ATTEMPTS_
:   number of attempts to flash the target (default: 3)

\-\-tag-filters _FILTERS_
:   comma-separated list of device tags used when allocating a target to specify required hardware or attributes [--tag-filters tag1,tag2]

-r, \-\-reset
:   force different type of reset

-C, \-\-program_cycle_s _TIMEOUT_
:   duration for program to sleep after copying binary onto target (default: 4)

-R, \-\-reset-timeout _TIMEOUT_
:   when forcing a reset (with -r), specify a post-reset idle delay in seconds (default: 1)

\-\-process-start-timeout _TIMEOUT_
:   set maximum time (seconds) to wait for an internal process to start (default: 60)

-e, \-\-enum-host-tests _DIR_
:   define directory with local host tests (default: ./test/host_tests)

\-\-test-cfg _FILE_
:   specify test class data about host test configuration

\-\-run
:   runs binary image on target (workflow: flash, reset, output console)

\-\-skip-flashing
:   skips use of copy/flash plugin. Note: target will not be reflashed

\-\-skip-reset
:   skips use of reset plugin. Note: target will not be reset

-P, \-\-polling-timeout _TIMEOUT_
:   timeout (seconds) for readiness of mount point and serial port of local or remote device (default: 60)

-b, \-\-send-break
:   send reset signal to board on specified port (-p PORT) and print serial output; can be combined with `-r RESET_TYPE` switch

\-\-baud-rate _BAUD\_RATE_
:   baud rate of target; overrides values from mbed-ls, disk/mount point (-d, \-\-disk-path), and serial port -p <port>:<baud rate>

-v, \-\-verbose
:   enable verbose mode

\-\-serial-output-file _FILE_
:   save target serial output to this file

\-\-compare-log _FILE_
:   log file to compare with the serial output from target

\-\-list
:   print registered host test and exit

\-\-plugins
:   print registered plugins and exit

\-\-version
:   print version and exit

\-\-help
:   print usage details and exit

# EXAMPLES

To flash the file `test.bin` to the mounted path `/mnt/MBED` on the `K64F` microcontroller, using the `shell` copy method with target serial port `COM5`, and sleeping for 5 seconds after flashing, run:

```
$ mbedhtrun --disk /mnt/MBED --port COM5 --image-path "test.bin" -C 4 --copy shell --micro K64F
```

# REPORTING BUGS

Upstream bug tracker: https://github.com/ARMmbed/htrun/issues

# COPYRIGHT

Copyright (c) 2011-2018 ARM Limited

# AUTHOR

This manual page is based on the mbed-host-tests documentation. It was created by Nick Morrott <nickm@debian.org> for the Debian GNU/Linux system, but may be used by others

# SEE ALSO

**yotta**(1), **mbed-test-wrapper**(1), **mbedflsh**(1)
